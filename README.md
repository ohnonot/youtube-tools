# youtube-tools

A collection of scripts around searching & streaming youtube without a browser.

### Scenario and possible workflow

Essentially I want a representation of the first page of results of a youtube search, without the obnoxious "I agree" popups that started overlaying everything recently.
That's what `youtube-search` is there for.
It will return familiar youtube page URLs (not the actual media URL) like "https://www.youtube.com/watch?v=XXXXXXXXXX" and copy those to the clipboard.
Then I use `mpv-clip` to grab that URL from the clipboard and make mpv+youtube-dl play it.

To avoid youtube kicking me out or delaying me for refusing to use their web pages in a browser, I run `youtube-search` through tor, with `mytorsocks.sh`, but stream the actual video through the "normal" WWW. That is why the above described clipboard solution works best for me.  
Nevertheless, it is possible to directly launch a program that deals with the URL (like a media player, or youtube-dl). When `youtube-search` was launched with `mytorsocks.sh` then this program will use the same tor connection.

`mpv-clip` also has an option to replace youtube.com with a mirror site such as those listed at [invidio.us][inv].

Here are two commands I have bound to two hotkeys, one to search, one to play:

    xterm -e 'mytorsocks.sh youtube-search -m 10 -- "$(xclip -o)"'
    sh -c 'mpv-clip -y invidious.snopyta.org "$(xclip -o)"'

All scripts have a '-h' option that will explain its options and list dependencies.

## youtube-search

A bash script that searches youtube for search terms entered on the command line.
Relies on `youtube-dl` (or a compatible executable), `perl` and the `HTML::Entities` module (part of the perl-html-parser package on ArchLinux) and `jshon`.

## mpv-clip

A wrapper around the glorious combination of [mpv][mpv] and [youtube-dl][ytdl] which can play much more than just youtube links.
It is meant to be used without a terminal, assigned to hotkeys, taking arguments from the clipboard & copying them there again, with metadata added.
Heavily sending desktop notifications (can be disabled).

## mytorsocks.sh

A wrapper script around [torsocks][ts] that

1. Starts tor on a given port.
2. Uses torsocks (on that port) to start any application in an isolated environment, i.e. all network connections go through torsocks.
3. Closes tor after torsocks & the application have terminated.

### A word about torsocks

`mytorsocks.sh` can be used with other applications, too. But torsocks itself does not work with all applications. My tests (Archlinux, Dec 2020) revealed that torsocks works with

- curl, wget, most network requests in scripts
- netsurf
- mpv
- youtube-dl

but does not work with

- ping ([an article][tsa] mentioned that this is because ping is setuid)
- falkon
- dillo
- firefox

Of course that list isn't complete.

## youtube-search.py

A python script that searches youtube for entered search terms. It does _not_ rely on youtube-dl.
It is only a matter of time until it stops working.
Kudos to teckk from LQ who put this in the public domain.

## Licensing

Everything in here is under the license described in the LICENSE file, unless otherwise mentioned.

## TODO

Somehow include thumbnails, possibly by wrapping the whole thing in a GUI like Yad or Zenity.

[mpv]: https://mpv.io/
[ytdl]: https://yt-dl.org/
[jsh]: https://github.com/keenerd/jshon
[inv]: https://invidio.us/
[ts]: https://gitweb.torproject.org/torsocks.git/
[tsa]: https://www.jamieweb.net/blog/tor-is-a-great-sysadmin-tool/
